package io.labtech.cloud.configserver;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"test", "native"})
public class TestStarter {
    private final ApplicationContext applicationContext;
    private final TestRestTemplate testRestTemplate;
    private final int localPort;
    private final String contextPath;

    public TestStarter(ApplicationContext applicationContext, @Autowired TestRestTemplate testRestTemplate, @LocalServerPort int localPort, @Value("${server.servlet.contextPath}") String contextPath) {
        this.applicationContext = applicationContext;
        this.testRestTemplate = testRestTemplate;
        this.localPort = localPort;
        this.contextPath = contextPath;
    }

    @Test
    @DisplayName("Test the application context")
    public void validateApplicationContext() {
        assertNotNull(this.applicationContext, "The application context is null :-(");
        assertNotNull(this.testRestTemplate, "The testRestTemplate object is null :-(");
        assertNotNull(this.contextPath, "The contextPath object is null :-(");
    }

    @Test
    @DisplayName("Test the actuator info endpoint")
    public void testActuatorInfoEndpoint() {
        testGenericUrl("/actuator/info");
    }

    @Test
    @DisplayName("Test the configuration server endpoint")
    public void testConfigServerEndpoint() {
        testGenericUrl("/configserver-native.yaml");
    }

    private void testGenericUrl(String url) {
        var genericUrl = "http://localhost:" + localPort + contextPath + url;
        var response = this.testRestTemplate.getForEntity(genericUrl, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody(), "The actuator info response is null :-(");
    }
}
