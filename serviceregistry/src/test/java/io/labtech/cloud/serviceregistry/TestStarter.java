package io.labtech.cloud.serviceregistry;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
            "management.server.port=60000"
        }
)
@ActiveProfiles({"test"})
public class TestStarter {

    private final ApplicationContext applicationContext;
    private final TestRestTemplate testRestTemplate;
    private final int localPort;
    private final String contextPath;

    public TestStarter(ApplicationContext applicationContext, @Autowired TestRestTemplate testRestTemplate, @LocalServerPort int localPort, @Value("${eureka.dashboard.path}") String contextPath) {
        this.applicationContext = applicationContext;
        this.testRestTemplate = testRestTemplate;
        this.localPort = localPort;
        this.contextPath = contextPath;
    }

    @Test
    @DisplayName("Test the application context")
    public void validateApplicationContext() {
        assertNotNull(this.applicationContext, "The application context is null :-(");
        assertNotNull(this.testRestTemplate, "The testRestTemplate object is null :-(");
        assertNotNull(this.contextPath, "The contextPath object is null :-(");
    }

    @Test
    @DisplayName("Test the actuator info endpoint")
    public void testActuatorInfoEndpoint() {
        testGenericUrl("/actuator/info", 60000);
    }

    @Test
    @DisplayName("Test the registry dashboard homepage")
    public void testRegistryDashboardHomepage() {
        testGenericUrl(this.contextPath);
    }

    private void testGenericUrl(String url) {
        this.testGenericUrl(url, this.localPort);
    }

    private void testGenericUrl(String url, int port) {
        var genericUrl = "http://localhost:" + port + url;
        var response = this.testRestTemplate.getForEntity(genericUrl, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody(), "The actuator info response is null :-(");
    }
}
